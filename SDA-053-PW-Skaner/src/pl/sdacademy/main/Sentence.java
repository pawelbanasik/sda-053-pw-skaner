package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sentence {
	
	public final String path1 = "src\\zad1\\";

	public String readSentence(String filename) {
		String sum = "";
		try (Scanner sc = new Scanner(new File(this.path1 + filename))) {

			while (sc.hasNextLine()) {

				sum += sc.next() + " ";

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		sum = sum.substring(0, 1).toUpperCase() + sum.substring(1);
		sum = sum.trim();
		if (!sum.endsWith(".")) {
			sum = sum.concat(".");
		}

		return sum;

	}

}
