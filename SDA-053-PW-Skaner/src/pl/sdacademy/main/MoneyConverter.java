package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {

	public final String path3 = "src\\zad3\\";

	public double readCourse(String currency) throws FileNotFoundException {
		
		if (currency.equalsIgnoreCase("PLN"))
			return 1;
		
		Scanner sc = new Scanner(new File(this.path3 + "currency.txt"));
		String text = "";
		double kurs =0;
		String[] arrayPomocniczy = new String[2];
		String[] arrayWalutaKurs = new String[2];
		
		while(sc.hasNextLine()){
			text = sc.nextLine();
			arrayPomocniczy = text.split(" ");
			arrayWalutaKurs = arrayPomocniczy[1].split("\t");
			if (arrayWalutaKurs[0].equalsIgnoreCase(currency)){
				kurs = ((Double.parseDouble(arrayWalutaKurs[1].replace(",", "."))))/(Double.parseDouble(arrayPomocniczy[0]));
			} 
			
		}
		
		for(int i =0; i < arrayPomocniczy.length;i++ ){
			System.out.println(i + arrayPomocniczy[i]);
			}
	
		
		System.out.println("*****************************");
	
		for(int i =0; i < arrayWalutaKurs.length;i++ ){
			System.out.println(i + arrayWalutaKurs[i]);
			}
		
		System.out.println("*************************");
		System.out.println(kurs);
		System.out.println("Koniec sprawdzaczy. Pod tym wynik.");
		System.out.println("*************************");
		return kurs;

	}

	public double convert(double money, String to) throws FileNotFoundException {
		return this.convert(money, to, "PLN");
	}

	// to moze byc zle ten dziwny przelicznik jakos mi nie pasuje ale zostawiam
	// bo to wiedza z zycia XD
	public double convert(double money, String to, String from) throws FileNotFoundException {
		double fromCourse = readCourse(from);
		double toCourse = readCourse(to);
		System.out.println(from + " " + fromCourse);
		System.out.println(to + " " + toCourse);
		return (fromCourse * money) * toCourse;
	}
}