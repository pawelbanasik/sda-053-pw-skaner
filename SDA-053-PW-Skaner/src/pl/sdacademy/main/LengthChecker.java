package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	public final String path5 = "src\\zad5\\";

	private boolean isProperLength(String arg, int len) {

		if (arg.length() > len) {
			return true;
		} else {
			return false;
		}

	}

	private String[] readFile(String fileName) {
		String text = "";
		String[] array = new String[50];

		try (Scanner sc = new Scanner(new File(this.path5 + fileName))) {
			int i = 0;
			while (sc.hasNextLine()) {
				text = sc.nextLine();
				array[i] = text;
				i++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

//		for (int j = 0; j < array.length; j++) {
//			System.out.println(array[j]);
//		}

		return array;

	}

	private void writeFile(String[] fileContent, int len) {

		try (PrintWriter pw = new PrintWriter(new FileWriter(new File(this.path5 + "word_X.txt"), true))) {

			for (int i = 0; i < fileContent.length; i++) {
				if (isProperLength(fileContent[i], len))
					pw.println(fileContent[i]);
			}
			pw.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void make(String fileInput, int len) {

		
		writeFile(readFile(fileInput), len);
		System.out.println("Slowa zosta�y zapisane w pliku.");
		// String[] array = readFile(fileInput);

		// for (int i = 0; i < array.length; i++) {
		// if (isProperLength(array[i], len)) {
		// writeFile(array, len);
		// }

		// }

		// for (int j = 0; j < array.length; j++){
		// System.out.println(array[j]);
		// }

	}

}
