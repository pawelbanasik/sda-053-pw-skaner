package pl.sdacademy.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	public final String path4 = "src\\zad4\\";
	
	public double toKelvin(double temp) {
		return temp - 273.15;
	}

	public double toFahrenheit(double temp) {
		return 9 / 5 * temp + 32;
	}

	public double[] readTemp(String filename) throws FileNotFoundException {

		Scanner sc = new Scanner(new File(this.path4 + filename));
		double[] array = new double[countLines("tempC.txt")];
		int i = 0;
		String text = "";
		while (sc.hasNextLine()) {

			text = sc.nextLine().replace(",", ".");

			array[i] = Double.parseDouble(text);
			i++;
		}

		for (int j = 0; j < array.length; j++) {
			System.out.println(array[j]);
		}

		return array;
	}

	public void writeTemp(double[] temp) {

		try {
			PrintWriter pw = new PrintWriter(new FileWriter(new File(this.path4 + "tempK.txt")));
			PrintWriter pw2 = new PrintWriter(new FileWriter(new File(this.path4 + "tempF.txt")));
			for (int i = 0; i < temp.length; i++) {
				pw.println(toKelvin(temp[i]));
				pw2.println(toFahrenheit(temp[i]));
			}
			pw.close();
			pw2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public int countLines(String filename) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(this.path4 + filename));
		int i = 0;
		String currentLine = "";
		while (sc.hasNextLine()) {
			currentLine = sc.nextLine();
			if (!currentLine.equals("")) {
				i++;
			}

		}
		sc.close();
		return i;
	}

}
