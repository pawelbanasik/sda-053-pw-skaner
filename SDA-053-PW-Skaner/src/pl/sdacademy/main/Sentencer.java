package pl.sdacademy.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Sentencer extends Sentence {

	public final String path2 = "src\\zad2\\";
	
	public void writeSentence(String filename, String sentence) {
		// to true tutaj spowoduje ze bedzie pisal kolejne linijki appenduje
		try (PrintWriter pw = new PrintWriter(new FileWriter(new File(this.path2 + filename), true))) {

			pw.println(sentence);
			pw.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
